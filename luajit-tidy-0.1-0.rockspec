package = "luajit-tidy"
version = "0.1-0"
source = {
	url = "",
}
description = {
	summary = "libtidy luajit binding.",
	detailed = [[
		[libtidy](https://www.html-tidy.org/) ffi library.
	]],
	homepage = "https://bitbucket.org/senanetworksinc/luajit-tidy",
	license = "MIT/X11"
}
dependencies = {
}
build = {
	type = "builtin",
	modules = {
		["luajit-tidy.init"] = "luajit-tidy/init.lua",
		["luajit-tidy.cdef"] = "luajit-tidy/cdef.lua",
	},
}
