# luajit-tidy

[libtidy](https://www.html-tidy.org/)をLuajit FFIで呼び出すモジュールです。

# 依存

[libtidy](https://www.html-tidy.org/) >= 5.6.0

Luajit

# インストール
`$ luarocks install luajit-tidy`

# 使い方
```lua
local ffi = require "ffi"
local tidy = require "luajit-tidy.init"

local input = "<title>Foo</title><p>Foo!"
local output = ffi.new("TidyBuffer", {})
local errbuf = ffi.new("TidyBuffer", {})

local tdoc = tidy.tidyCreate()

print(string.format("Tidying:\t%s\n", input))

local ok = tidy.tidyOptSetBool( tdoc, tidy.TidyXhtmlOut, true )  -- Convert to XHTML
local rc = -1

if ok then
	rc = tidy.tidySetErrorBuffer(tdoc, errbuf)      -- Capture diagnostics
end

if rc >= 0 then
	rc = tidy.tidyParseString(tdoc, input)           -- Parse the input
end

if rc >= 0 then
	rc = tidy.tidyCleanAndRepair(tdoc)               -- Tidy it up!
end

if rc >= 0 then
	rc = tidy.tidyRunDiagnostics(tdoc)               -- Kvetch
end

if rc > 1 then                                    -- If error, force output.
	local tmp = tidy.tidyOptSetBool(tdoc, tidy.TidyForceOutput, true)
	if tmp then
		rc = tmp
	else
		rc = -1
	end
end

if rc >= 0 then
	rc = tidy.tidySaveBuffer(tdoc, output)          -- Pretty Print
end

if rc >= 0 then
	if rc > 0 then
		print(string.format( "\nDiagnostics:\n\n%s", ffi.string(errbuf.bp)))
	end
	print(string.format( "\nAnd here is the result:\n\n%s", ffi.string(output.bp)))
else
	print(string.format( "A severe error (%d) occurred.\n", rc ))
end

tidy.tidyBufFree(output)
tidy.tidyBufFree(errbuf)
tidy.tidyRelease(tdoc)
```

# Revesion

* 2021/02/09 v0.1-0 release
